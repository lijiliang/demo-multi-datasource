package com.lee.test2.mapper;

import com.lee.test2.repository.Manager;
import org.apache.ibatis.annotations.Mapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ManagerMapperTest {

    private Logger logger = Logger.getLogger(ManagerMapperTest.class.getName());
    @Autowired
    private ManagerMapper managerMapper;
    @Test
    void findAll() {
        var all = managerMapper.findAll();
        Assertions.assertEquals(2, all.size());
        all.forEach(i -> logger.info(i.toString()));
    }
}