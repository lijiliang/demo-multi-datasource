package com.lee.test2.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.logging.Logger;


@SpringBootTest
class UserRepositoryTest {
    Logger logger = Logger.getLogger(ManagerRepository.class.getName());

    @Autowired
    private ManagerRepository managerRepository;

    @Test
    public void testQueryFromTest2() {
        var all = managerRepository.findAll();
        Assertions.assertEquals(2, all.size());
        all.forEach(i -> logger.info(i.toString()));
    }
}