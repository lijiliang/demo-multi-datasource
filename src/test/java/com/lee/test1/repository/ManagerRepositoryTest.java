package com.lee.test1.repository;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.logging.Logger;

@SpringBootTest
class ManagerRepositoryTest {

    Logger logger = Logger.getLogger(UserRepository.class.getName());

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testQueryFromTest1() {
        var all = userRepository.findAll();
        Assertions.assertEquals(2, all.size());
        all.forEach(i -> logger.info(i.toString()));
    }
}