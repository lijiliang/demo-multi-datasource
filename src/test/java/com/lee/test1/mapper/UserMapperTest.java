package com.lee.test1.mapper;

import com.lee.test1.repository.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.logging.Logger;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserMapperTest {

    Logger logger = Logger.getLogger(UserMapperTest.class.getName());
    @Autowired
    private UserMapper userMapper;
    @Test
    void findAll() {
        var all = userMapper.findAll();
        Assertions.assertEquals(2, all.size());
        all.forEach(i -> logger.info(i.toString()));
    }
}