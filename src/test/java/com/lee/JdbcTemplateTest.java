package com.lee;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;


/**
 * jdbc模板测试
 *
 * @author winston
 * @date 2022/04/20
 */
@SpringBootTest
public class JdbcTemplateTest {

    @Autowired
    private JdbcTemplate primaryJdbcTemplate;
    @Autowired
    private JdbcTemplate secondaryJdbcTemplate;

    @BeforeEach
    public void clean() {
        primaryJdbcTemplate.update("delete from user");
        secondaryJdbcTemplate.update("delete from user");
    }

    @Test
    public void testInsert() {
        primaryJdbcTemplate.update("insert into user(id, name) values (?, ?)", 1, "张三");
        primaryJdbcTemplate.update("insert into user(id, name) values (?, ?)", 2, "张三2");
        secondaryJdbcTemplate.update("insert into user(id, name) values (?, ?)", 1, "李四");
        secondaryJdbcTemplate.update("insert into user(id, name) values (?, ?)", 2, "李四2");

        Assertions.assertEquals(2, primaryJdbcTemplate.queryForObject("select count(*) from user", Integer.class));
        Assertions.assertEquals(2, secondaryJdbcTemplate.queryForObject("select count(*) from user", Integer.class));
    }
}
