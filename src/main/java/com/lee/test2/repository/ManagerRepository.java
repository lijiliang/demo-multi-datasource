package com.lee.test2.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Description TODO
 * @Author winston
 * @DateTime 2022/4/21
 */
@Repository
public interface ManagerRepository extends JpaRepository<Manager, Long> {

}
