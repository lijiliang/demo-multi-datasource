package com.lee.test2.mapper;

import com.lee.test2.repository.Manager;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description TODO
 * @Author winston
 * @DateTime 2022/4/21
 */
@Mapper
public interface ManagerMapper {

    @Select("select * from manager")
    List<Manager> findAll();
}
