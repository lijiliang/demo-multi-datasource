package com.lee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoMultiDatasourceApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoMultiDatasourceApplication.class, args);
    }

}
