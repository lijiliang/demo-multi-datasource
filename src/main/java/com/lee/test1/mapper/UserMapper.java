package com.lee.test1.mapper;

import com.lee.test1.repository.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Description TODO
 * @Author winston
 * @DateTime 2022/4/21
 */
@Mapper
public interface UserMapper {

    @Select("select * from user")
    List<User> findAll();
}
