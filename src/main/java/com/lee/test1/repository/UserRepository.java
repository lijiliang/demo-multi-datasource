package com.lee.test1.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Description TODO
 * @Author winston
 * @DateTime 2022/4/21
 */
@Repository
public interface UserRepository  extends JpaRepository<User, Long> {

}
